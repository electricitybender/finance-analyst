Project targeting private use. Contact the author for more information or clarification.

You need pandas for development and running locally, and pyinstaller if you want to export to an executable. Run `pyinstaller --onefile analyst.py` in the shell to create an .exe file. I may be missing background dependencies here, forgive me if this causes issues.

No license granted. All rights reserved.
