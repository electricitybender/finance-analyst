"""
    Author: Alexander Petrov
"""
import logging
import os
import pandas as pd


def main():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    file_handler = logging.FileHandler('FinancialReportAnalyst.log')
    logger.addHandler(file_handler)

    logger.info("Program starting.")

    csv_files = [file for file in os.listdir(os.getcwd()) if file[-4:] == ".csv" and "LatestReport" not in file]

    for file in csv_files:
        csv = pd.read_csv(file)
        csv.dropna(axis=0, how='all', inplace=True)
        csv.reset_index(inplace=True)
        csv.drop('index', axis=1, inplace=True)

        dataframes = []
        ticket_types = []
        booking_fee = []
        payments = []
        discounts = []

        films = csv[csv['Film'].notnull()]
        locs = films.index.tolist()
        # we will now handle multiple films per sheet
        for i in range(len(films) - 1):
            dataframes.append(csv.iloc[locs[i]:locs[i + 1]])
            ticket_types.append(dataframes[i]['Ticket'].value_counts().drop('Total Entry'))
            payments.append(dataframes[i]['Amount'].dropna().apply(lambda x: x.strip('£')).apply(float))
            booking_fee.append(payments[i][payments[-1] > 0] * 0.014 + 0.2)  # this is the formula for booking fees
            discounts.append(dataframes[i].dropna(subset=['Discount']).loc[dataframes[i]['Discount'] != 0])
            discounts[i] = discounts[i][discounts[i]['Ticket'] != 'Total Entry']

        # this solves the last film
        # but, if there is just one overall, this will solve it
        dataframes.append(csv.iloc[locs[-1]:-1])
        ticket_types.append(csv.iloc[locs[-1]:-1]['Ticket'].value_counts().drop('Total Entry'))
        payments.append(csv.iloc[locs[-1]:-1]['Amount'].dropna().apply(lambda x: x.strip('£')).apply(float))
        booking_fee.append(payments[-1][payments[-1] > 0] * 0.014 + 0.2)  # this is the formula for booking fees
        discounts.append(csv.iloc[locs[-1]:-1].dropna(subset=['Discount']).loc[csv.iloc[locs[-1]:-1]['Discount'] != 0])
        discounts[-1] = discounts[-1][discounts[-1]['Ticket'] != 'Total Entry']

        columns = {}
        unrecognized = []
        for i in range(len(films)):
            if not (payments[i].sum() / 2) == payments[i].iloc[-1]:
                logger.error("Something went wrong with the calculated income. Cannot verify values!\n"
                             "Film: %s; Calculated income: %f; Provided income: %f"
                             % (films.iloc[i]['Film'], payments[i].sum() / 2, payments[i].iloc[-1]))
                raise ValueError

            a = 0
            b = 0
            c = 0
            for ticket in ticket_types[i].index.tolist():
                if ticket == 'Early Bird Student':
                    a += ticket_types[i][ticket]
                    a -= len(discounts[i][discounts[i]['Ticket'] == ticket])
                elif ticket == 'Early Bird Phoenix':
                    a += ticket_types[i][ticket]
                    a -= len(discounts[i][discounts[i]['Ticket'] == ticket])
                elif ticket == 'Standard Student':
                    b += ticket_types[i][ticket]
                    b -= len(discounts[i][discounts[i]['Ticket'] == ticket])
                elif ticket == 'Standard Phoenix':
                    b += ticket_types[i][ticket]
                    b -= len(discounts[i][discounts[i]['Ticket'] == ticket])
                elif ticket == 'Early Bird External':
                    b += ticket_types[i][ticket]
                    b -= len(discounts[i][discounts[i]['Ticket'] == ticket])
                elif ticket == 'Standard External':
                    c += ticket_types[i][ticket]
                    c -= len(discounts[i][discounts[i]['Ticket'] == ticket])
                else:
                    if ticket not in unrecognized:
                        unrecognized.append(ticket)

            data = {'Total Income, £': payments[i].iloc[:-1].sum(),
                    'Booking fees, £': booking_fee[i].iloc[:-1].sum(),
                    'Number of £3 tickets': a,
                    'Number of £4 tickets': b,
                    'Number of £5 tickets': c,
                    '£3 ticket income': a * 3,
                    '£4 ticket income': b * 4,
                    '£5 ticket income': c * 5,
                    '"Discount" tickets': len(discounts[i])
                    }
            for ticket in ticket_types[i].index.tolist():
                data[ticket] = ticket_types[i][ticket]

            columns[films.iloc[i]['Film']] = pd.Series(data)

            if unrecognized:
                logger.warning("The following ticket types were not recognized:\n" + str(unrecognized))

        frame = pd.DataFrame(data=columns).reindex(pd.Series(data).index)

        frame.to_csv("LatestReport_" + file)

    logger.info("Program finished.")
    logging.shutdown()


if __name__ == '__main__':
    main()

